package com.gabriel.fm.di

import com.gabriel.fm.FutureMindApplication
import com.gabriel.fm.activities.ListActivity
import com.gabriel.fm.net.NetModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by gabrieljanucik on 01.04.2017.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class, NetModule::class))
interface ApplicationComponent {
    fun inject(application: FutureMindApplication)

    fun inject(listActivity: ListActivity)
}