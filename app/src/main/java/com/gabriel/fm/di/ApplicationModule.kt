package com.gabriel.fm.di

import android.content.Context
import com.gabriel.fm.FutureMindApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by gabrieljanucik on 01.04.2017.
 */
@Module
class ApplicationModule(private val futureMindApplication: FutureMindApplication) {

    @Provides
    @Singleton
    @ForApplication
    fun provideApplicationContext(): Context {
        return futureMindApplication
    }

    @Provides
    @Singleton
    fun provideFutureMindApplication(): FutureMindApplication {
        return futureMindApplication
    }
}