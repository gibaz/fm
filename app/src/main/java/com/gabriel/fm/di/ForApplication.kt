package com.gabriel.fm.di

import javax.inject.Qualifier

/**
 * Created by gabrieljanucik on 01.04.2017.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ForApplication