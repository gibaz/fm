package com.gabriel.fm.net

import com.gabriel.fm.FutureMindApplication
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by gabrieljanucik on 01.04.2017.
 */
@Module
class NetModule {

    private val BASE_URL = " http://pinky.futuremind.com/"

    @Provides
    @Singleton
    fun provideHttpCache(futureMindApplication: FutureMindApplication): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(futureMindApplication.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache) : OkHttpClient {
        val timeoutInSeconds = 30
        return OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(timeoutInSeconds.toLong(), TimeUnit.SECONDS)
                .readTimeout(timeoutInSeconds.toLong(), TimeUnit.SECONDS)
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): NetworkService {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(NetworkService::class.java)
    }
}