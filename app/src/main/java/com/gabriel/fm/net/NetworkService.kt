package com.gabriel.fm.net

import com.gabriel.fm.model.Response
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by gabrieljanucik on 01.04.2017.
 */
interface NetworkService {

    @GET("~dpaluch/test35/")
    fun getList(): Single<Response>
}