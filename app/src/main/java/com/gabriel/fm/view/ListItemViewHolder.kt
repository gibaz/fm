package com.gabriel.fm.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.gabriel.fm.R
import com.gabriel.fm.kotterknife.bindView
import com.gabriel.fm.model.ListItem
import com.squareup.picasso.Picasso

/**
 * Created by gabrieljanucik on 01.04.2017.
 */
class ListItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    var onItemSelectedListener: ((listItem: ListItem) -> Unit)? = null

    val image: ImageView by bindView(R.id.image)
    val title: TextView by bindView(R.id.title)
    val date: TextView by bindView(R.id.modification_date)
    val description: TextView by bindView(R.id.description)

    fun displayItem(item: ListItem) {
        view.setOnClickListener{ view -> onItemSelectedListener?.invoke(item)}
        Picasso.with(view.context).load(item.imageUrl).into(image)
        title.text = item.title
        date.text = item.getFormattedDate()
        description.text = item.getProperlyDescription()
    }
}