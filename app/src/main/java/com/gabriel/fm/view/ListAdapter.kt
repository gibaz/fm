package com.gabriel.fm.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.gabriel.fm.R
import com.gabriel.fm.model.ListItem

/**
 * Created by gabrieljanucik on 01.04.2017.
 */
class ListAdapter : RecyclerView.Adapter<ListItemViewHolder>() {

    private var items: MutableList<ListItem> = mutableListOf()
    private val layoutResId = R.layout.item_list
    private var onItemSelectedListener: ((listItem: ListItem) -> Unit)? = null

    override fun onBindViewHolder(holder: ListItemViewHolder, position: Int) {
        holder.displayItem(items[position])
        holder.onItemSelectedListener = onItemSelectedListener
        showAnimation(holder.view, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemViewHolder {
        return ListItemViewHolder(LayoutInflater.from(parent.context).inflate(layoutResId, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setOnItemClickListener(onItemSelectedListener: ((listItem: ListItem) -> Unit)) {
        this.onItemSelectedListener = onItemSelectedListener
        notifyDataSetChanged()
    }

    fun setData(listItems: List<ListItem>) {
        items.clear()
        items.addAll(listItems)
        notifyDataSetChanged()
    }

    fun showAnimation(view: View, position: Int) {
        view.startAnimation(AnimationUtils.loadAnimation(view.context, android.R.anim.slide_in_left))
    }
}