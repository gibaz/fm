package com.gabriel.fm.activities

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.widget.ViewAnimator
import com.gabriel.fm.FutureMindApplication
import com.gabriel.fm.R
import com.gabriel.fm.kotterknife.bindView
import com.gabriel.fm.model.ListItem
import com.gabriel.fm.net.NetworkService
import com.gabriel.fm.view.ListAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ListActivity : AppCompatActivity() {

    @Inject
    lateinit var networkService: NetworkService

    val animator: ViewAnimator by bindView(R.id.view_animator)
    val recyclerView: RecyclerView by bindView(R.id.recycler_view)
    val swipeRefresh: SwipeRefreshLayout by bindView(R.id.swipe_refresh)

    lateinit var adapter: ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        FutureMindApplication.graph.inject(this)
        initView()
        loadList()
    }

    fun initView() {
        showProgress()
        swipeRefresh.setOnRefreshListener { refreshIfNotRefreshing() }
        adapter = ListAdapter()
        adapter.setOnItemClickListener { item -> itemClicked(item) }
        recyclerView.adapter = adapter
    }

    fun loadList() {
        networkService.getList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ response -> showList(response.data) },
                        { loadList() })
    }

    fun refreshIfNotRefreshing() {
        loadList()
    }

    fun showList(list: List<ListItem>) {
        showContent()
        if (swipeRefresh.isRefreshing) {
            swipeRefresh.isRefreshing = false
        }
        adapter.setData(list)
    }

    fun itemClicked(listItem: ListItem) {
        startActivity(DetailActivity.getIntent(this, listItem.getDescriptionUrl()))
    }

    fun showProgress() {
        animator.displayedChild = PROGRESS_BAR
    }

    fun showContent() {
        animator.displayedChild = CONTENT
    }

    companion object {
        private const val CONTENT = 0
        private const val PROGRESS_BAR = 1
    }
}
