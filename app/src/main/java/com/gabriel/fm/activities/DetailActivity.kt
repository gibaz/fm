package com.gabriel.fm.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebView
import com.gabriel.fm.R
import com.gabriel.fm.kotterknife.bindView

/**
 * Created by gabrieljanucik on 02.04.2017.
 */
class DetailActivity : AppCompatActivity() {

    lateinit var url: String
    val webView: WebView by bindView(R.id.web_view)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        url = intent.extras.getString(ITEM_URL)
        webView.loadUrl(url)
    }

    companion object {
        fun getIntent(activity: AppCompatActivity, url: String): Intent {
            return Intent(activity, DetailActivity::class.java)
                    .putExtra(ITEM_URL, url)
        }

        val ITEM_URL = "detail_item_url"
    }
}