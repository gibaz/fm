package com.gabriel.fm.model

import com.google.gson.annotations.SerializedName

/**
 * Created by gabrieljanucik on 02.04.2017.
 */
data class Response(@SerializedName("data") val data: List<ListItem>)