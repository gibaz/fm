package com.gabriel.fm.model

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat

/**
 * Created by gabrieljanucik on 01.04.2017.
 */
data class ListItem(
        @SerializedName("title") val title: String,
        @SerializedName("description") val description: String,
        @SerializedName("orderId") val id: Long,
        @SerializedName("modificationDate") val modificationDate: String,
        @SerializedName("image_url") val imageUrl: String
) {

    fun getDescriptionUrl() : String {
        return description.substring(description.indexOf("http"))
    }

    fun getProperlyDescription() : String {
        return description.substring(0, description.indexOf("http"))
    }

    fun getFormattedDate() : String {
        var formatter = SimpleDateFormat("yyyy-MM-dd")
        var date = formatter.parse(modificationDate)
        var userFriendlyFormatter = SimpleDateFormat("dd MMM yyyy")
        return userFriendlyFormatter.format(date)
    }
}