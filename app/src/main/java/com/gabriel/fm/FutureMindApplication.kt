package com.gabriel.fm

import android.app.Application
import com.gabriel.fm.di.ApplicationComponent
import com.gabriel.fm.di.ApplicationModule
import com.gabriel.fm.di.DaggerApplicationComponent

/**
 * Created by gabrieljanucik on 01.04.2017.
 */
class FutureMindApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        graph = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
        graph.inject(this)
    }

    companion object {
        @JvmStatic lateinit var graph: ApplicationComponent
    }
}